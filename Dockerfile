FROM golang:alpine

RUN apk update && \
    apk add git make

WORKDIR /go/src/monGOes

COPY ./Gopkg.lock ./Gopkg.lock
COPY ./Gopkg.toml ./Gopkg.toml

RUN go get github.com/golang/dep/cmd/dep
RUN dep ensure --vendor-only

COPY . .

RUN make build
CMD ["make", "start"]