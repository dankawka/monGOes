package monGOes

import (
	"os"

	log "github.com/sirupsen/logrus"
)

var logger = log.New()

func init() {
	logLevel := os.Getenv("LOG_LEVEL")

	switch logLevel {
	case "DEBUG":
		{
			logger.Level = log.DebugLevel
		}
	case "INFO":
		{
			logger.Level = log.InfoLevel
		}
	case "WARNING":
		{
			logger.Level = log.WarnLevel
		}
	case "ERROR":
		{
			logger.Level = log.ErrorLevel
		}
	case "FATAL":
		{
			logger.Level = log.FatalLevel
		}
	case "PANIC":
		{
			logger.Level = log.PanicLevel
		}
	default:
		{
			logger.Level = log.WarnLevel
		}
	}

	logger.Out = os.Stdout
}
