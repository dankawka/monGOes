package monGOes

import (
	"time"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func getDataFromMongo(c *mgo.Collection, updateFieldName string, limit, rangeStart int) ([]map[string]interface{}, int) {
	query := c.Find(bson.M{updateFieldName: bson.M{"$gt": rangeStart}}).Limit(limit)

	var result []map[string]interface{}
	err := query.All(&result)

	if err != nil {
		logger.WithFields(log.Fields{
			"collectionName":  c.Name,
			"databaseName":    c.Database.Name,
			"updateFieldName": updateFieldName,
			"limit":           limit,
			"rangeStart":      rangeStart,
			"error":           err,
		}).Panic("Could not query MongoDB")
	}

	var last int
	if len(result) == 0 {
		last = rangeStart
	} else {
		last = result[len(result)-1][updateFieldName].(int)
	}

	logger.WithFields(log.Fields{
		"collectionName": c.Name,
		"databaseName":   c.Database.Name,
		"recordsFound":   len(result),
		"limit":          limit,
		"rangeStart":     rangeStart,
		"last":           last,
	}).Info("Got response from MongoDB")

	return result, last
}

func pollData(c *mgo.Collection, es *elastic.Client, rc resourceConfig) {
	last := 0
	var data []map[string]interface{}
	for {
		data, last = getDataFromMongo(c, rc.MongoUpdateField, rc.Limit, last)
		if len(data) > 0 {
			PutDataToES(data, es, rc.ElasticsearchIndex, rc.ElasticsearchType)
		}
		time.Sleep(time.Duration(rc.PollingInterval) * time.Millisecond)
	}
}
