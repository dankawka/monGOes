package monGOes

import (
	"io/ioutil"
	"os"
)

func getConfig() ([]byte, error) {
	configFilePath := os.Getenv("CONFIG_FILE_PATH")

	logger.Debug("CONFIG_FILE_PATH=", configFilePath)

	file, err := ioutil.ReadFile(configFilePath)
	return file, err
}
