package monGOes

import (
	"encoding/json"
	"sync"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

type elasticsearchConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Endpoint string `json:"endpoint"`
}

type mongoDBConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Endpoint string `json:"endpoint"`
}

type resourceConfig struct {
	MongoDatabaseName   string `json:"mongoDatabaseName"`
	MongoCollectionName string `json:"mongoCollectionName"`
	MongoUpdateField    string `json:"mongoUpdateField"`
	ElasticsearchIndex  string `json:"elasticsearchIndex"`
	ElasticsearchType   string `json:"elasticsearchType"`
	PollingInterval     int    `json:"pollingInterval"`
	Limit               int    `json:"limit"`
}

type config []struct {
	Elasticsearch elasticsearchConfig `json:"elasticsearch"`
	Mongodb       mongoDBConfig       `json:"mongodb"`
	Resources     []resourceConfig    `json:"resources"`
}

func RunPoller() {
	configFile, err := getConfig()

	if err != nil {
		logger.WithFields(log.Fields{
			"error": err,
		}).Panic("Failed to load config file")
	}

	config := config{}
	err = json.Unmarshal(configFile, &config)

	if err != nil {
		logger.WithFields(log.Fields{
			"error": err,
		}).Panic("Failed to unmarshall config file")
	}

	var wg sync.WaitGroup

	logger.WithFields(log.Fields{
		"configsFound": len(config),
	}).Debug("Successfully loaded config. Starting poller")

	for _, cfg := range config {
		mongoSession, err := createMongoSession(cfg.Mongodb.Endpoint)

		if err != nil {
			logger.WithFields(log.Fields{
				"endpoint": cfg.Mongodb.Endpoint,
				"error":    err,
			}).Panic("Could not create MongoDB client")
		}

		es, err := elastic.NewClient(
			elastic.SetURL(cfg.Elasticsearch.Endpoint),
		)

		if err != nil {
			logger.WithFields(log.Fields{
				"error": err,
			}).Panic("Could not create ES client")
		}

		logger.WithFields(log.Fields{
			"resourcesFound": len(cfg.Resources),
		}).Debug("Starting processing resources")

		for _, resource := range cfg.Resources {
			collection := mongoSession.DB(resource.MongoDatabaseName).C(resource.MongoCollectionName)
			wg.Add(1)
			go pollData(collection, es, resource)
		}
	}

	wg.Wait()
}
