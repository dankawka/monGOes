package monGOes

import mgo "gopkg.in/mgo.v2"

func createMongoSession(host string) (*mgo.Session, error) {
	session, err := mgo.Dial(host)
	return session, err
}
