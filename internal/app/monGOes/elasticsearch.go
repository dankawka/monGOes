package monGOes

import (
	"context"

	"gopkg.in/mgo.v2/bson"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

func PutDataToES(data []map[string]interface{}, es *elastic.Client, index, esType string) {
	bulkRequest := es.Bulk()

	for _, el := range data {
		id := el["_id"].(bson.ObjectId)
		stringID := id.Hex()

		// _id field is forbidden in ES
		delete(el, "_id")

		indexRequest := elastic.NewBulkIndexRequest().Index(index).Type(esType).Id(stringID).Doc(el)
		bulkRequest = bulkRequest.Add(indexRequest)
	}

	if bulkRequest.NumberOfActions() > 0 {
		bulkResponse, err := bulkRequest.Do(context.Background())
		if err != nil {
			panic(err)
		}

		failedResults := bulkResponse.Failed()
		succeededResults := bulkResponse.Succeeded()

		logger.WithFields(log.Fields{
			"index":            index,
			"type":             esType,
			"failedResults":    len(failedResults),
			"succeededResults": len(succeededResults),
		}).Info("Bulk response from Elasticsearch")

		if len(failedResults) > 0 {
			logger.WithFields(log.Fields{
				"failedResults": len(failedResults),
			}).Error("Got errors from Elasticsearch")

			for _, fail := range failedResults {
				logger.WithFields(log.Fields{
					"id":    fail.Id,
					"index": fail.Index,
					"type":  fail.Type,
					"error": fail.Error,
				}).Error("Error from Elasticsearch")
			}
		}
	}

}
