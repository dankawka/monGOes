DOCKER_REGISTRY	?= dkawka
TAG ?= 1.0.0

build:
	go build -o ./build/monGOes cmd/monGOes/main.go

start:
	./build/monGOes

build-image:
	docker build -t $(DOCKER_REGISTRY)/mongoes:$(TAG) .

push-image:
	docker push $(DOCKER_REGISTRY)/mongoes:$(TAG)

.PHONY: build
